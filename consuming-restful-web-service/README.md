# Consuming a RESTful Web Service

- Build an application that uses Spring's `RestTemplate` to retrieve a random Spring Boot quotation from [here](http://gturnquist-quoters.cfapps.io/api/random)

- Build and run with:

```
./gradlew build

java -jar build/libs/gs-consuming-rest-0.1.0.jar
```
