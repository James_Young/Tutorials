# Using Spring for Scheduling Tasks

Displays the current time every 5 seconds.

To run:

    ./gradlew build
    java -jar build/libs/gs-scheduling-tasks-0.1.0.jar


