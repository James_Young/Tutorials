import re
import getpass

minCharacters = 8
rMinChars = re.compile(r'.{8,}')
rHasUpperCase = re.compile(r'[A-Z]')
rHasLowerCase = re.compile(r'[a-z]')
rHasNumeric = re.compile(r'[0-9]')

password_tests = {
    "min. " + str(minCharacters) : rMinChars,
    "upper case" : rHasUpperCase,
    "lower case" : rHasLowerCase,
    "has numeric" : rHasNumeric
        }
if __name__ == "__main__":
    password = getpass.getpass("Password: ")
    passwordStrength = len(password_tests)
    for testName, test in password_tests.items():
        if test.search(password) == None:
            print("Password needs {} characters".format(testName))
            passwordStrength -= 1
    strength = ""
    if passwordStrength / len(password_tests) < 0.5:
        strength = "poor"
    elif passwordStrength / len(password_tests) < 0.65:
        strength = "weak"
    elif passwordStrength / len(password_tests) < 0.85:
        strength = "ok"
    elif passwordStrength / len(password_tests) < 1.0:
        strength = "good"
    else: 
        strength = "excellent"
    print("Password strength is :", strength)
    


   
