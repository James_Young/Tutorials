# Strong Password Detection

Use of regex patterns to assess the strength of a password.

To run, open the terminal and type:

    python passwordStrength.py

Then fill in the password you want to assess
